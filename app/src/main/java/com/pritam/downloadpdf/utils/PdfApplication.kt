package com.pritam.downloadpdf.utils

import android.app.Application
import android.content.Context

class PdfApplication : Application() {
    companion object {
        lateinit var mContext: PdfApplication
    }
    override fun onCreate() {
        super.onCreate()
        mContext = this
    }

    fun getAppContext(): Context? {
        return mContext
    }
}