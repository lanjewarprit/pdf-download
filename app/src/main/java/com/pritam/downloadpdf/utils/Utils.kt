package com.pritam.downloadpdf.utils

import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import android.os.Environment
import androidx.appcompat.app.AlertDialog
import com.pritam.newsapp.interfaces.OnRetryListener
import java.io.File

object Utils {

    /**
     * alert pop up here
     */
    fun alertPopUp(
        mContext: Context?,
        title: String?,
        message: String?,
        onRetryListener: OnRetryListener?
    ) {
        val alert = AlertDialog.Builder(
            mContext!!
        )
        alert.setTitle(title)
        alert.setMessage(message)
        alert.setCancelable(false)
        alert.setPositiveButton(
            "Ok"
        ) { dialog, whichButton -> onRetryListener?.retry() }
        alert.show()
    }

    fun getFilePath() : String {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
            return ""+ File(Environment.getDownloadCacheDirectory().path)
        else {
            val contexWrap = ContextWrapper(PdfApplication.mContext)
            return  ""+contexWrap.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!
        }
    }
}