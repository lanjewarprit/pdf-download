package com.pritam.newsapp.interfaces

interface OnRetryListener {
    fun retry()
}