package com.pritam.downloadpdf.viewmodel

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Base64
import androidx.core.content.FileProvider
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pritam.downloadpdf.utils.Utils
import java.io.BufferedReader
import java.io.File
import java.io.FileOutputStream


class PdfDownloadViewModel : ViewModel() {
    var fileName : String = ""
     val fileDownloaded = MutableLiveData<Boolean>()

    private lateinit var  context : Context

    companion object{
        private const val TAG = "PdfDownloadViewModel"
    }

    /**
     * get file from assert folder
     * byte array are stored in txt file
     */
    fun downloadPdf(mainActivity: Context) {
        context = mainActivity
        downloadPdfInLocalStorage(readAsset(context,"MyName.txt"))
    }

    /**
     * open pdf file
     */
     fun loadFile() {
        try {
            val PATH = getFilePath().toString()
            val file = File("$PATH/MyTask")

            val pdfFile = File(file, fileName)
            if (!pdfFile.exists()) {
                Utils.alertPopUp(
                    context,
                    "Info",
                    "File does not exist",
                    null
                )
                return
            }
            val path: Uri = getUriFromFile(pdfFile)
            val pdfOpenintent = Intent(Intent.ACTION_VIEW)
            pdfOpenintent.flags =
                Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_ACTIVITY_CLEAR_TOP
            pdfOpenintent.setDataAndType(path, "application/pdf")
           context. startActivity(pdfOpenintent)
        } catch (e: ActivityNotFoundException) {
            Utils.alertPopUp(
                context,
                "Info",
                "File open filed",
                null
            )
        }
    }

    /**
     * get file uri
     */
    private fun getUriFromFile(file: File): Uri {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Uri.fromFile(file)
        } else {
            FileProvider.getUriForFile(
                context,
                context.getApplicationContext().getPackageName().toString() + ".provider",
                file
            )
        }
    }

    /**
     * read file from assert folder
     */
    fun readAsset(context: Context, fileName: String): String =
        context
            .assets
            .open(fileName)
            .bufferedReader()
            .use(BufferedReader::readText)

    /**
     * download pdf here
     *
     * @param responseData
     */
    private fun downloadPdfInLocalStorage(responseData: String) {
        val path = getFilePath().toString()
        val directory = File("$path/MyTask")
        directory.mkdirs()
        val fname = "TestDummy" + ".pdf"
        val file = File(directory, fname)
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            out.write(Base64.decode(responseData, Base64.NO_WRAP))
            out.flush()
            out.close()
            fileName = file.name
            fileDownloaded.value = true
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * get file path as per android version
     * @return
     */
    fun getFilePath(): File? {
        var documentDirectory: File? = null
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            documentDirectory = File(Environment.getExternalStorageDirectory().path)
        } else {
            val contextWrapper = ContextWrapper(context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                documentDirectory =
                    contextWrapper.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
            }
        }
        return documentDirectory
    }
}