package com.pritam.downloadpdf.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.pritam.downloadpdf.R
import com.pritam.downloadpdf.databinding.ActivityMainBinding
import com.pritam.downloadpdf.viewmodel.PdfDownloadViewModel

class MainActivity : AppCompatActivity() {
    var activitySubBooksBinding: ActivityMainBinding? = null
    var subBooksViewModel: PdfDownloadViewModel? = null
    private val PERMISSION_REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        clickListener()
        observeUser()
    }

    /**
     * initialise view binding here
     */
    private fun initView() {
        activitySubBooksBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activitySubBooksBinding!!.root)
        subBooksViewModel = ViewModelProvider(this)[PdfDownloadViewModel::class.java]
    }

    /**
     * get file name
     */
    private fun observeUser() {
        subBooksViewModel!!.fileDownloaded.observe(this, Observer{
            it?.let {
                activitySubBooksBinding!!.btnOpen.visibility = View.VISIBLE
            }
        })
    }

    /**
     * click listener here
     */
    private fun clickListener() {
        activitySubBooksBinding!!.btnDownload.setOnClickListener{
            if (checkPermission()) {
                subBooksViewModel!!.downloadPdf(this)
            } else {
                requestPermission()
            }
        }
        activitySubBooksBinding!!.btnOpen.setOnClickListener{
            if (checkPermission()) {
                subBooksViewModel!!.loadFile()
            } else {
                requestPermission()
            }
        }
    }

    /**
     * local file permission
     */
    private fun checkPermission(): Boolean {
        val result: Int = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        return result == PackageManager.PERMISSION_GRANTED
    }

    /**
     * request permission here
     */
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                subBooksViewModel!!.downloadPdf(this)
            } else {
                Snackbar.make(
                    activitySubBooksBinding!!.coordinatorLayout,
                    "Permission Denied, Please allow to proceed !", Snackbar.LENGTH_LONG
                ).show()
            }
        }

    }

}